import math

reveal_type(math.pi)  # Revealed type is 'builtins.float'


def headline(
        text,       # type: str
        align,      # type: bool
    ):                 # type: (...) -> str
    if align:
        return f"{text.title()}\n{'-' * len(text)}"
    else:
        return f" {text.title()} ".center(50, "o")


def circumference(radius):
    # type: (float) -> float
    return 2 * math.pi * radius


print(headline("python type checking"))
print(headline("use mypy", align=True))
radius = 1
circumference(radius)
reveal_locals()
