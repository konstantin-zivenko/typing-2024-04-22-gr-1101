import math
from typing import reveal_type

reveal_type(math.pi)

radius = 1

circumference = 2 * math.pi * radius
reveal_locals()
