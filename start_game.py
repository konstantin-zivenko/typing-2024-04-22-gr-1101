from typing import Dict, List, Tuple, Sequence
import random

Card = tuple[str, str]
Deck = list[Card]


SUITS = "♠ ♡ ♢ ♣".split()
RANKS = "2 3 4 5 6 7 8 9 10 J Q K A".split()

# names: list = ["Petro", "Olga"]
# version: tuple = (3, 12, 2)
# options: dict = {"centered": False, "capitalize": True}

# names: List[str] = ["Petro", "Olga"]
# ages: list[int] = [22, 43, 97]
#
# version: Tuple[int, int, int] = (3, 12, 2)
# level: tuple[int, int, int] = (10, 12, 1)
#
# options: Dict[str, bool] = {"centered": False, "capitalize": True}
# features: dict[str, bool] = {"centered": False, "capitalize": True}


def create_deck(shuffle: bool = False) -> Deck:
    """Create a new deck of 52 cards"""
    deck = [(s, r) for r in RANKS for s in SUITS]
    if shuffle:
        random.shuffle(deck)
    return deck


def deal_hands(
        deck: Deck
) -> tuple[
    Deck, Deck, Deck, Deck
]:
    """Deal the cards in the deck into four hands"""
    return deck[0::4], deck[1::4], deck[2::4], deck[3::4]


def play():
    """Play a 4-player card game"""
    deck = create_deck(shuffle=True)
    names = "P1 P2 P3 P4".split()
    hands = {n: h for n, h in zip(names, deal_hands(deck))}

    for name, cards in hands.items():
        card_str = " ".join(f"{s}{r}" for (s, r) in cards)
        print(f"{name}: {card_str}")


if __name__ == "__main__":
    play()